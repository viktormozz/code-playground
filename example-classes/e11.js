class Slider{

    constructor(
        id,
        sliderWidth,
        sliderHeight,
        lista_elementos,
        navegador,
        estilo_navegador,
        paginador,
        estilo_paginador,
        tiempo_animacion,
        estilo_animacion,
        auto
    ){
        this.id = id;
        this.sliderWidth = sliderWidth;
        this.sliderHeight = sliderHeight;
        this.lista_elementos = lista_elementos;
        this.navegador = navegador;
        this.estilo_navegador = estilo_navegador;
        this.paginador = paginador;
        this.estilo_paginador = estilo_paginador;
        this.tiempo_animacion = tiempo_animacion;
        this.estilo_animacion = estilo_animacion;
        this.auto = auto;
        

        this.imageIdx = 0;
        this.sliderInterval = 0;

        this.setupActions();
    }

    setupActions(){
        console.log('setup is on the way!');
        this.setSliderDimensions();
        this.setSliderNav();
        this.setSliderImages();
        this.setSliderEvents();
        this.setSliderAuto();
    }

    setSliderDimensions()
    {
        let sliderCanvas = document.querySelector(`#${this.id}`);
        //TAREA2: guardar ancho y alto
        sliderCanvas.setAttribute('style', `width: ${this.sliderWidth}px; height: ${this.sliderHeight}px;`);
        console.log(sliderCanvas);
    }

    setSliderNav()
    {
        if(this.lista_elementos.length > 1){
            //colocar flechas de navegacion
            document.querySelector(`#${this.id} .slider-nav`).style.display = 'block';
        }

        //TAREA1: dinamizar el tamaño del contenedor
        let sliderCanvasInner = document.querySelector(`#${this.id} .slider-inner`);
        //sliderCanvasInner.setAttribute('style', `width: ${this.sliderWidth*this.lista_elementos.length}px`);
        sliderCanvasInner.style.width = `${this.sliderWidth*this.lista_elementos.length}px`;

    }

    setSliderImages()
    {
        console.log('qty: ', this.lista_elementos);

        this.lista_elementos.forEach( (image) => { //bucle para leer imagen x imagen
            let currentimage = document.createElement("IMG");
            currentimage.setAttribute("src", image);
            currentimage.setAttribute("width", "780");
            currentimage.setAttribute("height", "500");
            document.querySelector(`#${this.id} .slider-inner`).appendChild(currentimage);
        });

    }

    setSliderEvents()
    {
        // definir click en boton izquierdo
        //TAREA3: ESTABLECER CLICK AL BOTON IZQUIERDO
        document.querySelector(`#${this.id} .slider-nav-prev`).addEventListener('click', () => {
            this.setSliderInnerPosition(this.imageIdx-=1);
            //TAREA4: VALIDAR EL INICIO DE LAS IMAGENES.

        });

        // definir click en boton derecho
        document.querySelector(`#${this.id} .slider-nav-next`).addEventListener('click', () => {
            this.setSliderInnerPosition(this.imageIdx+=1);
            //TAREA4: VALIDAR EL FINAL DE LAS IMAGENES.
        });

    }

    setSliderInnerPosition(slider_position)
    {
        // this.imageIdx = 
        //     (slider_position < 0) ? 
        //         this.lista_elementos.length - 1 : 
        //         (slider_position >= this.lista_elementos.length) ? 
        //             0 : 
        //             slider_position;

        if(slider_position < 0)
        {
            this.imageIdx = this.lista_elementos.length - 1;
        }
        else if(slider_position >= this.lista_elementos.length) 
        {
            this.imageIdx = 0;
        }
        else
        {
            this.imageIdx = slider_position;
        }

        document.querySelector(`#${this.id} .slider-inner`).style.left = `-${this.imageIdx*this.sliderWidth}px`;
    }

    //TAREA5: HACERLO AUTOMATICO SI FUERA EXPLICITO.
    setSliderAuto()
    {
        if(this.auto)
        {
            this.sliderInterval = setInterval( 
                                    function(){ 
                                        this.setSliderInnerPosition(this.imageIdx+=1); 
                                    }.bind(this), 
                                    2000);
        }
    }
    //TAREA6: ESTABLECER EL PAGINADOR INFERIOR.

    //TAREA7: ESTABLECER EL CLICK A CADA BOTONCITO DEL PAGINADOR.

}

let sliderPrincipal = new Slider( 
    'slider_principal',
    '780', 
    '500', 
    ['image-1.jpg', 'image-2.jpg', 'image-3.jpg', 'image-2.jpg'], 
    true, 
    'square', 
    true, 
    'square', 
    0.5, 
    'linear', 
    true
);



