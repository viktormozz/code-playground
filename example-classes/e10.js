class perkins {
    constructor() {

      this.window_options = 0
      this.carousels = [
        { 
          uid: 'cyber_category', 
          ele: 0,
          items: 1, 
          options: {
            animateOut: 'fadeOut',
            autoPlay: false,        
            dots: false,
            loop: false,
            nav: true,
            navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
            responsive: {
              0: {
                padding: 30,
                items: 1,
              },
              768: {
                items: 2,
              }
            }

          },
          on_desktop: false
        }
      ]

      this.setupActions();

    }

    getWindowSize() {

      let w = window,
        d = document,
        e = d.documentElement,
        g = d.getElementsByTagName('body')[0],
        x = w.innerWidth || e.clientWidth || g.clientWidth,
        y = w.innerHeight|| e.clientHeight|| g.clientHeight;
      return { w: x, h: y };
    }

    setupActions() {

      this.window_options = this.getWindowSize();
      this.setCarousels();
      this.setScrollNavigatorListener();
      this.setAnchorsLinks();

      
    }

    setCarousels() {

      this.carousels.forEach( (carousel, idx) => {

        if( this.window_options.w < 1170 ){
          carousel.ele = this.initCarousel(carousel.uid, carousel.options )
        }
      })

    }

    setAnchorsLinks(){

      $('#cyber_nav a, #cyber_nav_fixed a').on('click', function(e){

        e.preventDefault();
        let ancla=$(this).attr('href')
        ancla=ancla.substring(1);
        let position=$('#'+ancla).position();
        $('html, body').animate({scrollTop: (position.top - 75)}, 1000);

      });
    }
    
    initCarousel(carousel_uid, carousel_opts) {

      document.querySelector(`#${carousel_uid}`).classList.add('owl-carousel')
      return $(`#${carousel_uid}`).owlCarousel(carousel_opts);

    }

    setScrollNavigatorListener(){

      window.addEventListener('scroll', this.checkScrollNavigator.bind(this));
      
    }

    
    checkScrollNavigator(e){

      let last_known_scroll_position = window.scrollY;

      let cybernav_rect = document.querySelector('#cyber_nav').getBoundingClientRect(),
        categories_rect = document.querySelector('#cyber_category').getBoundingClientRect(),
        categories_pos_y = categories_rect.top,
        categories_carousel_options = {
          animateOut: 'fadeOut',
          autoWidth: true,        
          dots: false,
          loop: false,
          nav: true,
          navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
        }; 
      

      if(categories_rect.top <= 0)
      {
        document.querySelector('#cyber_nav_fixed').style.display = 'block';
        if(this.window_options.w < 992)
        {
          this.initCarousel('cyber_nav_fixed', categories_carousel_options);
        }
      }
      else
      {
        document.querySelector('#cyber_nav_fixed').style.display = 'none';
      }

      $('.nav-link').removeClass('nav-link-selected');

      this.checkMenuSection();

    }

    checkMenuSection(){

      let navElements = document.querySelectorAll('#cyber_nav_fixed a');

      navElements.forEach((navElement) => {

        let block_title_rect = document.querySelector(`${navElement.getAttributeNode('href').value}`).getBoundingClientRect();

        if(block_title_rect.top <= 120)
        {
          this.clearMenuSections();
          navElement.classList.add('nav-link-selected');
        }
      });

    }

    clearMenuSections(){
      let navElements = document.querySelectorAll('#cyber_nav_fixed a');
      navElements.forEach((navElement) => {
          navElement.classList.remove('nav-link-selected');
      });
    }
  }

  return new perkins();
