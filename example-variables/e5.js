let abc = 2; //varible dentro del bloque o contexto
const persona = {
    nombre: 'Juan',
    apellido: 'Perez',
    edad: 15
}; // constante crear una referencia de solo-lectura al objeto creado y no puede ser modificado

if(abc == 2)
{
    /*si bien es cierto no podemos modificar la referencia y su estructura si podemos 
    alterar los contenidos que estan dentro de la estructura del objeto.*/
    persona.nombre = 'Jose';
    persona.apellido = 'Aguilar';
    persona.edad = 14;
}

console.log(persona);