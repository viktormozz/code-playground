let abc = 2; //varible dentro del bloque o contexto
const persona = {
    nombre: 'Juan',
    apellido: 'Perez',
    edad: 15
}; // constante crear una referencia de solo-lectura al objeto creado y no puede ser modificado

if(abc == 2)
{
    persona = {
        nombre: 'Jose',
        apellido: 'Aguilar',
        edad: 20
    };  
}

console.log(persona);