let abc = 2; //varible dentro del bloque o contexto
const persona = {
    nombre: 'Juan',
    apellido: 'Perez',
    edad: 15
}; // constante crear una referencia de solo-lectura al objeto creado y no puede ser cambiada

if(abc == 2)
{
    /*si bien es cierto no podemos modificar la referencia, si podemos 
    alterar los contenidos que estan dentro de la estructura del objeto e inclusive la estructura .*/
    persona.nombre = 'Jose';
    persona.apellido = 'Aguilar';
    persona.edad = 14;
    persona.genero = 'M';
}

console.log(persona);