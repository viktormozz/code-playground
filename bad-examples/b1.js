$(document).ready(function(a) {
    a(function() {
      a(".lazy").lazy({
        combined: !0,
        delay: 500,
        afterLoad: function(o) {
          a("#site-body img").removeClass("lazy");
        }
      });
    }),
      a("#imgloading").fadeOut("0"),
      a(".bigbanowl").owlCarousel({
        items: 1,
        animateOut: "fadeOut",
        loop: !0,
        margin: 0,
        pagination: !0,
        nav: false,
        autoplay: !0,
        autoplayHoverPause: !0,
        smartSpeed: 250
      }),
      a(".horimenu").owlCarousel({
        stagePadding: 60,
        singleItem: !0,
        pagination: !0,
        autoplay: !1,
        autoplayHoverPause: !1,
        smartSpeed: 450,
        loop: !1,
        responsiveClass: !0,
        responsive: {
          0: {
            items: 5,
            stagePadding: 20,
            loop: !1,
            dots: !1,
            nav: !0
          },
          600: {
            nav: !0,
            stagePadding: 60,
            items: 5,
            dots: !1,
            nav: !0
          }
        }
      }),
      a(".owlmicarousel").owlCarousel({
        animateOut: "fadeOut",
        singleItem: !0,
        pagination: !0,
        autoplay: !1,
        autoplayHoverPause: !1,
        smartSpeed: 450,
        loop: !1,
        margin: 10,
        responsiveClass: !0,
        responsive: {
          0: {
            items: 2,
            loop: !1,
            dots: !1,
            nav: !0
          },
          600: {
            nav: !0,
            items: 2,
            dots: !1,
            nav: !0
          },
          1e3: {
            items: 2,
            nav: !0,
            loop: !1,
            dots: !1,
            margin: 20
          }
        }
      }),
      a(".owltucarousel").owlCarousel({
        animateOut: "fadeOut",
        singleItem: !0,
        pagination: !0,
        autoplay: !1,
        autoplayHoverPause: !1,
        smartSpeed: 450,
        loop: !1,
        margin: 10,
        responsiveClass: !0,
        responsive: {
          0: {
            items: 1,
            loop: !1,
            dots: !1,
            nav: !0
          },
          600: {
            nav: !0,
            items: 1,
            dots: !1,
            nav: !0
          },
          1e3: {
            items: 1,
            nav: !0,
            loop: !1,
            dots: !1,
            margin: 20
          }
        }
      }),
      a(".owlcaromarca").owlCarousel({
        singleItem: !0,
        pagination: !0,
        autoplay: !1,
        autoplayHoverPause: !1,
        smartSpeed: 450,
        loop: !0,
        margin: 10,
        responsiveClass: !0,
        responsive: {
          0: {
            items: 1,
            loop: !1,
            dots: !1,
            nav: !0
          },
          600: {
            nav: !0,
            items: 2,
            dots: !1,
            nav: !0
          }
        }
      }),
      a(".owlpreciazos").owlCarousel({
        animateOut: "fadeOut",
        singleItem: !0,
        pagination: !0,
        autoplay: !1,
        autoplayHoverPause: !1,
        smartSpeed: 450,
        loop: !0,
        margin: 10,
        responsiveClass: !0,
        responsive: {
          0: {
            items: 2,
            dots: !1,
            nav: !0
          },
          600: {
            items: 2,
            dots: !1,
            nav: !0
          },
          1e3: {
            items: 4,
            nav: !0,
            loop: !0,
            dots: !1,
            margin: 20
          }
        }
      }),
      a(".owlcategorycar").owlCarousel({
        stagePadding: 100,
        pagination: !0,
        autoplay: !1,
        autoplayHoverPause: !1,
        smartSpeed: 350,
        items: 1,
        nav: !0,
        loop: !1,
        margin: 10,
        responsiveClass: !0,
        responsive: {
          0: {
            items: 1,
            stagePadding: 60,
            dots: !0,
            nav: !1
          },
          600: {
            items: 2,
            stagePadding: 100,
            dots: !0,
            nav: !1
          }
        }
      });
  
    $(".catdestowl").owlCarousel({
      animateOut: "fadeOut",
      singleItem: true,
      pagination: true,
      autoplay: true,
      autoplayHoverPause: true,
      smartSpeed: 450,
      loop: true,
      margin: 10,
      responsiveClass: true,
      responsive: {
        0: {
          items: 2,
          dots: false,
          nav: true
        },
        600: {
          items: 2,
          dots: false,
          nav: true
        },
        1000: {
          items: 4,
          nav: true,
          dots: false,
          margin: 20
        }
      }
    });
  }),
    $(document).ready(function() {
      $(window).scroll(function() {
        var a = $(window).scrollTop(),
          o = window.matchMedia("(min-width: 768px)"),
          e = window.matchMedia("(max-width: 767px)");
        o.matches &&
          (a > 600
            ? $("#cmpldmenu").addClass("fijarmenudesk")
            : $("#cmpldmenu").removeClass("fijarmenudesk")),
          e.matches &&
            (a > 700
              ? ($("#menutop").addClass("fijarmenudesk"),
                $("#menutop").removeClass("hide"))
              : ($("#menutop").addClass("hide"),
                $("#menutop").removeClass("fijarmenudesk")));
      });
    }),
    $("#countdown").countdown("2021/04/10", function(a) {
      $(this).html(
        a.strftime(
          '<div class="countdown-time"><span>%H</span><span>%M</span><span>%S</span></div>'
        )
      );
    });
  
  $(function() {
    $(".btnleermas").click(function() {
      $(".txtfootermas").toggle();
    });
  });
  
  